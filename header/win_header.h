//BY MUDREAM
#ifndef INC_WIN_HEADER_H
#define INC_WIN_HEADER_H
#define STRING_LENMAX 40
#define ARRAY_SIZEMAX 40

#define KEY_UP 72
#define KEY_DOWN 80
#define KEY_LEFT 75
#define KEY_RIGHT 78

enum MMCOLOR
{
	WCOLOR_BLACK = 0,
	WCOLOR_RED,
	WCOLOR_GREEN,
	WCOLOR_BLUE,
	WCOLOR_WHIT,
};

void Randomize(bool DEBUG = 0);
int Rnd(int ps);

void ClearScreen();
void Gotoxy(int x,int y);
void Setcolor(int bg,int fc);

void Sleep(int ms);

int MakeList(char list[ARRAY_SIZEMAX][STRING_LENMAX],int cnt);

#endif
