#include"win_header.h"
#include<time.h>
#include<stdio.h>
#include<string.h>
#include<conio.h>
#include<windows.h>
#include<stdlib.h>
//#include<dos.h>
void Randomize(int DEBUG)
{
	if(DEBUG==0)
		srand(time(NULL));
	else
		srand(1);
}
int Rnd(int ps)
{
	if(ps<=1) return 0;
	return rand()%ps;	
}
void ClearScreen()
{
	system("cls");
}
void Gotoxy(int x,int y)
{
	COORD scrn;
	HANDLE hOuput = GetStdHandle(STD_OUTPUT_HANDLE);
	
	scrn.X = x, scrn.Y = y;

	SetConsoleCursorPosition(hOuput,scrn);
}

void Setcolor(int bg,int fc)
{
	//TODO:simplify 
	//?BLEND
	if(fc==WCOLOR_RED) fc = FOREGROUND_RED;
	else if(fc==WCOLOR_BLUE) fc = FOREGROUND_BLUE;
	else if(fc==WCOLOR_GREEN) fc = FOREGROUND_GREEN;
	else fc = 0;
	
	if(fc==WCOLOR_RED) fc = BACKGROUND_RED;
	else if(fc==WCOLOR_BLUE) fc = BACKGROUND_BLUE;
	else if(fc==WCOLOR_GREEN) fc = BACKGROUND_GREEN;
	else fc = 0;

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), bg | fc); 
	
}
void Sleep(int ms)
{
	_sleep(ms);
}
int MakeList(char list[ARRAY_SIZEMAX][STRING_LENMAX],int cnt)
{
	system("cls");
	if(cnt==0) return -1;
	int lenmax=0;
	int select = 0;
	while(1)
	{
		//Should Optimize
		Gotoxy(0,0);
		for(int lx=0;lx<cnt;lx++)
		{
			if(lx==select) printf("->%s\n",list[lx]);
			else  printf("  %s\n",list[lx]);
		}
		int c=getch();
		if(c==72)
		{
			if(select>0) 
				select--;
		}
		else if(c==80)
		{
			if(select<cnt-1)
				select++;
		}
		else if(c==13)
			return select;
		else if(c==27)
			return -1;
	}
}

