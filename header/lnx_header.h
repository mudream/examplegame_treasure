//BY MUDREAM
#ifndef INC_LNX_HEADER_H
#define INC_LNX_HEADER_H
#define STRING_LENMAX 40
#define ARRAY_SIZEMAX 40
enum MMCOLOR
{
	WCOLOR_BLACK = 0,
	WCOLOR_RED,
	WCOLOR_GREEN,
	WCOLOR_BLUE,
	WCOLOR_WHIT,
};

void Randomize(int DEBUG = 0);
int Rnd(int ps);

void ClearScreen();
void Gotoxy(int x,int y);
void Setcolor(int bg,int fc);

//For getch()


char getch();
char getche();

//void Sleep(int ms);

int MakeList(char list[ARRAY_SIZEMAX][STRING_LENMAX],int cnt);

#endif
