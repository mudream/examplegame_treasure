# 小遊戲

原始碼：<https://github.com/mudream4869/Treasure>

##  資料夾結構

1. header/
	
	學員們需要的標頭檔
 
1. problems/

	放置衍生出的問題

1. excode/ 

	放置範例程式碼和說明

1. lecture/

	放置教學簡報+檔案

## 完成能力需求
1. 基礎需求

	- 基本IO 判斷式 陣列 迴圈 函數

2. 額外部分(這部分提供標頭檔)
	
	這部分需要一個library (有LINUX)
	
	- 亂數函數
		
		* 提供DEBUG功能
					
				void Randomize(int DEBUG = 0);
				int Rnd(int ps);

	- IO Control 
	
		(已找：<https://github.com/Fernando-Lafeta/Biblioteca-Conio-2>)

		* 輸出位置控制
				
				void Gotoxy(int x,int y);
	
		* 螢幕控制
		
				void ClearScreen();

		* 顏色控制(額外)
				
				void Setcolor(int bg,int fc);
 
	- 時間函數
		
		* 如sleep()等待函數
			
				void Sleep(int ms);

	- 選單功能
		
		* 只做基礎功能，若要更華麗的選單，請學員繼續探索
		
				void MakeList(char list[][],int cnt);

## 主要部分

1. 來源
	
	來自 Mother Load 

	(參考 <http://www.gagameme.com/tw/game/mother-load.html>)

2. 遊戲架構
	
	1. 兩個方案

		- LOAD較小
		
			保留部分參數，簡化原本遊戲。
			
			* 油箱大小 		
			* 背包大小		
			* 金屬偵測範圍

		- LOAD較大
	
			全部參數：增加的部分因為跟*kbhit*的控制有關，所以需要一起增加，遊戲才能平衡，但也因此，LOAD會加重
		
			* 油箱大小
			* 背包大小
			* 金屬偵測範圍
			* 鑽頭硬度   
			* 血量
			* 風扇強度 
		
	2. 遊戲功能部分(有序)
		
		- 主要
		
			* 建立金屬資料庫 *Var, Array*
			* 參數管理	*Var, Array*
			* 顯示地圖	*printf, for*
			* 上下左右鍵移動	*getch*
			* 商店(交易、升級)	*清單*
			* 亂數地圖	*rand*
		
		- 次要
		
			* 檔案管理 *File IO*
			* 劇情
			* 特殊工具
			* 分數計算
			* 增加元素: "水"

3. 題類
	
	見 problems 資料夾

	- Loop
	- Array
	- String  
	- function	
	