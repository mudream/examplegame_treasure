#include<stdio.h>
#include<stdlib.h>
#include<time.h>
void MMoutput(const int *a,char *get);
int MMinput(const char *a,int *get);

void generate(int *);
void modify1(char *);
bool compare(int *,int *);
const int max_char = 1<<20;
char myget[max_char];

int main()
{
	srand(time(NULL));
	bool judge = true;
	for(int lx=0;(lx<100000)&&judge;lx++)
	{
		int line[5];
		int res[5];
		generate(line);
		MMoutput(line,myget);
		int flag = rand()%2;
		if(flag)
		{
			modify1(myget);
			int re = MMinput(myget,res);
			if(re==0) judge = false;
		}
		else
		{
			int re = MMinput(myget,res);
			if((compare(res,line)==0) || re)
				judge = false;
		}
	}
	if(judge)
		printf("AC!!!_____1718932347\n");
	else
		printf("WA!!!_____2938472983\n");
	return 0;
}
void generate(int *aa)
{
	for(int lx=0;lx<5;lx++)
		aa[lx] = rand()%10000;
}
bool compare(int *aa,int *bb)
{
	int flag = 1;
	for(int lx=0;(lx<5)&&flag;lx++)
		if(aa[lx]!=bb[lx])
			flag = 0;
	return flag;
}
void modify1(char *str)
{
	for(int lx=0;str[lx]!='\0';lx++)
		if(str[lx]!=' ')
			break;
	str[lx]=' ';
}
