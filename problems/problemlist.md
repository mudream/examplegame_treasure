# 題目s

1. 給一個地圖，和一堆指令，輸出最後的位置

	- 用到：二維陣列

	- 題目敘述：小雪很喜歡玩遙控機器人，她把機器人放在一個箱子裡，然後用上下左右的指令遙控她它。   給一個NXN的01矩陣(0表示可通過，1表示不能通過)、遙控機器的座標(x,y)和一連串的指令(由U,D,L,R組成，分別代表上下左右)，問最終停在哪個格子。
		
		範例輸入：

			3	
			1 0 0 
			0 0 0
			0 1 0
			2 1
			RR

		範例輸出：
	
			3 1

1. 給一個地圖，和一堆指令，輸出每次的畫面

	- 用到：二維陣列

	- 題目敘述：小雪很喜歡玩遙控機器人，她把機器人放在一個箱子裡，然後用上下左右的指令遙控她它。   給一個NXN(N>5)的01矩陣(0表示可通過，1表示不能通過)和一連串的指令(由U,D,L,R組成)，每次命令後輸出。輸出時並不是將整個NXN印出來，而是打印出一個5X5的矩陣(請參考下面說明)
	
	- 怎麼印：首先要的參數有兩個：**當前的座標**、和**從哪一點開始打印的座標(以下稱作原點)**，然後稱有效移動若且惟若這個點沒移出NXN且沒移動到"1"上面。
		
		* 若當前座標向下合法移動，且當前座標距離打印邊界小於2、原點往下移動不會超出邊界，則原點往下移動。

		* 上、左、右也是一樣的判斷，原點必須位於NXN以內

		* 一開始原點設在(1,1),初始位置也設在(1,1)

		* 當前位置要印2
		
		
		輸入說明：第一行有個正整數 20>N>5，第二行開始會有N行，每行會有N個數字{0,1}，第N+2行則是一個由{U,D,L,R}組成的字串。

		範例輸入：

			9	
			0 0 0 0 0 0 0 0 0
			0 0 0 1 1 1 0 0 1
			0 1 0 0 1 0 0 0 1
			0 1 0 0 1 0 0 1 0
			1 0 1 0 0 0 1 0 0
			1 0 0 0 0 0 0 0 0 
			1 0 0 0 1 0 1 0 0 
			0 0 0 0 0 0 0 0 0
			1 0 0 1 0 0 1 0 1
			RRRRDR

		範例輸出：
	
			02000
			00011
			01001
			01001
			10100

			00200
			00011
			01001
			01001
			10100

			00020
			00011
			01001
			01001
			10100

			00020
			00111
			10010
			10010
			01000
			
			00020
			00111
			10010
			10010
			01000

			00020
			01110
			00100
			00100
			10001
			
1. 給一個地圖，和起點終點，輸出最短路徑長度

	- 用到：BFS
	
	- 題目敘述：

		範例輸入：

			5 4 
			1 0 0 0 0
			0 1 1 1 0
			0 0 0 0 0
			0 0 1 1 1
			1 2
			2 1
		
		範例輸出：
	
			10 

1. 給一個檔名(包含副檔名)，把副檔名改成給定的

	- 用到：字串陣列

	- 題目敘述：小夢在處理檔案遇到很大的困難，現在她有一堆副檔名各不相同的檔案，她想把他們的附檔名統一改成"**.out**"，但檔案數量太多了>A<。給一堆檔名，輸出改換後的(完整的)檔名(**若遇到如gal.exe.swp等多重附檔名，請改成gal.out**)，副檔名長度>=3。
		
		範例輸入：
			
			pal1.exe
			pal2.swp
			pal3.cpp
			pal4.h
			swp1.ds
		
		範例輸出：
			
			pal1.out
			pal2.out
			pal3.out
			pal4.out
			swp1.out

1. 想一個方法，保護好存檔內容(Special Judge)
	
	- 用到：綜合
   
	- 題目敘述：當在寫存檔遊戲時，很重要的是避免修改器的出現。只須完成兩個函數，和把gametest.h include ：
	 	
			#include "specjudge.h"
			void MMoutput(const int *a,char *get);
			int MMinput(const char *a,int *get);

		說明：
		
		* MMoutput要完成的事：遊戲裡有5個整數(0~9999)(由a給定，從a[0]~a[4])，你必須把他們輸出成一個檔案，當然，不會叫你直接用file IO,請輸出到 get裡 

		* MMinput要完成的事：讀入一個遊戲存檔(這裡你要由I讀取)，然後判斷這檔案是否遭修改過，若有，回傳1，否則，則將100個參數回傳至get。
		
		* MMoutput最多會被呼叫 ? 次

		* MMinpput最多會被呼叫 ? 次
	
		一個會WA的程式：
			
			#include <stdlib.h>
			#include "specjudge.h"
			void MMoutput(const int *a,char *get){
				get[0]='\0';
				return;
			}
			int MMinput(const char *I){
				return (I!='\0');
			}

		一個會TLE的程式

			#include <stdlib.h>
			#include "specjudge.h"
			
			void MMoutput(const int *a,char *get){
				//把丟進來的東西存起來
			}
			int MMinput(const char *I){
				//判斷是否有Output過
			}